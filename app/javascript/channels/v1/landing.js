function drawBinanceRates(rates) {
    let binanceRates = rates.binance

    $('#binance-rates #sell-out').html(binanceRates.sell_out);
    $('#binance-rates #buy-out').html(binanceRates.buy_out);
    $('#binance-rates #sell').html(binanceRates.sell_average);
    $('#binance-rates #buy').html(binanceRates.buy_average);
    $('#binance-rates #depth-sell').html(binanceRates.sell_depth);
    $('#binance-rates #depth-buy').html(binanceRates.buy_depth);
}

function drawPoloniexRates(rates) {
    let poloniexRates = rates.poloniex

    $('#poloniex-rates #sell-out').html(poloniexRates.sell_out);
    $('#poloniex-rates #buy-out').html(poloniexRates.buy_out);
    $('#poloniex-rates #sell').html(poloniexRates.sell_average);
    $('#poloniex-rates #buy').html(poloniexRates.buy_average);
    $('#poloniex-rates #depth-sell').html(poloniexRates.sell_depth);
    $('#poloniex-rates #depth-buy').html(poloniexRates.buy_depth);
}

function drawYoBitRates(rates) {
    let yoBitRates = rates.yo_bit

    $('#yo-bit-rates #sell-out').html(yoBitRates.sell_out);
    $('#yo-bit-rates #buy-out').html(yoBitRates.buy_out);
    $('#yo-bit-rates #sell').html(yoBitRates.sell_average);
    $('#yo-bit-rates #buy').html(yoBitRates.buy_average);
    $('#yo-bit-rates #depth-sell').html(yoBitRates.sell_depth);
    $('#yo-bit-rates #depth-buy').html(yoBitRates.buy_depth);
}

function drawLiveCoinRates(rates) {
    let liveCoinRates = rates.live_coin

    $('#live-coin-rates #sell-out').html(liveCoinRates.sell_out);
    $('#live-coin-rates #buy-out').html(liveCoinRates.buy_out);
    $('#live-coin-rates #sell').html(liveCoinRates.sell_average);
    $('#live-coin-rates #buy').html(liveCoinRates.buy_average);
    $('#live-coin-rates #depth-sell').html(liveCoinRates.sell_depth);
    $('#live-coin-rates #depth-buy').html(liveCoinRates.buy_depth);
}

function drawExmoRates(rates) {
    let exmoRates = rates.exmo

    $('#exmo-rates #sell-out').html(exmoRates.sell_out);
    $('#exmo-rates #buy-out').html(exmoRates.buy_out);
    $('#exmo-rates #sell').html(exmoRates.sell_average);
    $('#exmo-rates #buy').html(exmoRates.buy_average);
    $('#exmo-rates #depth-sell').html(exmoRates.sell_depth);
    $('#exmo-rates #depth-buy').html(exmoRates.buy_depth);
}

export default function rates(data) {
    let allRates = JSON.parse(data.rates_hash);

    drawBinanceRates(allRates);
    drawPoloniexRates(allRates);
    drawYoBitRates(allRates);
    drawLiveCoinRates(allRates);
    drawExmoRates(allRates);
}
