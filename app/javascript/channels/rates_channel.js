import consumer from "./consumer"
import rates from "./v1/landing"

consumer.subscriptions.create("RatesChannel", {
  connected() {
    // Called when the subscription is ready for use on the server
  },

  disconnected() {
    // Called when the subscription has been terminated by the server
  },

  received(data) {
    rates(data)
  }
});
