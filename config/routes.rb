Rails.application.routes.draw do
  root to: 'v1/landing#index'

  namespace :v1 do
    get 'rates/index'
  end
end
