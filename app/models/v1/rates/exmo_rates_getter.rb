# frozen_string_literal: true

module V1
  module Rates
    class ExmoRatesGetter < RatesBase
      include HTTParty
      base_uri 'https://api.exmo.com/v1'
      CURRENCY_PAIR = 'BTC_USD'.freeze

      private

      def api_rates
        api_data = call_rates

        {
          sell: api_data['ask'].map { |ask| ask[0..1].map(&:to_f) },
          buy: api_data['bid'].map { |bid| bid[0..1].map(&:to_f) }
        }
      end

      private

      def call_rates
        rates = self.class.get(rates_path, query: query)
        validate_response(rates)

        response_body = rates.parsed_response[CURRENCY_PAIR]
        validate_response_body('error', response_body)
        validate_data_integrity(response_body, 'ask', 'bid')

        response_body
      end

      def rates_path
        '/order_book/'
      end

      def query
        {
          limit: 15,
          pair: CURRENCY_PAIR
        }
      end
    end
  end
end
