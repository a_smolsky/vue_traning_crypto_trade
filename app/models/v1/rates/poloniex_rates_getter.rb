# frozen_string_literal: true

module V1
  module Rates
    class PoloniexRatesGetter < RatesBase
      private

      include HTTParty
      base_uri 'https://poloniex.com'
      CURRENCY_PAIR = 'USDT_BTC'.freeze

      private

      def api_rates
        api_data = call_rates

        {
          sell: api_data['asks'].map { |ask| ask[0..1].map(&:to_f) },
          buy: api_data['bids'].map { |bid| bid[0..1].map(&:to_f) }
        }
      end

      private

      def call_rates
        rates = self.class.get(rates_path, query: query)
        validate_response(rates)

        response_body = rates.parsed_response
        validate_response_body('error', response_body)
        validate_data_integrity(response_body)

        response_body
      end

      def rates_path
        '/public'
      end

      def query
        {
          depth: 15,
          currencyPair: CURRENCY_PAIR,
          command: 'returnOrderBook'
        }
      end
    end
  end
end
