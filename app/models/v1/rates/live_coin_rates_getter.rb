# frozen_string_literal: true

module V1
  module Rates
    class LiveCoinRatesGetter < RatesBase
      include HTTParty
      base_uri 'https://api.livecoin.net'
      CURRENCY_PAIR = 'BTC/USD'.freeze

      private

      def api_rates
        api_data = call_rates

        {
          sell: api_data['asks'].map { |ask| ask[0..1].map(&:to_f) },
          buy: api_data['bids'].map { |bid| bid[0..1].map(&:to_f) }
        }
      end

      private

      def call_rates
        rates = self.class.get(rates_path, query: query)
        validate_response(rates)

        response_body = rates.parsed_response
        validate_response_body('errorMessage', response_body)
        validate_data_integrity(response_body)

        response_body
      end

      def rates_path
        '/exchange/order_book'
      end

      def query
        {
          depth: 15,
          currencyPair: CURRENCY_PAIR
        }
      end
    end
  end
end
