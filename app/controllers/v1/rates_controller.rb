module V1
  class RatesController < ApplicationController
    def index
      all_rates = get_rates
      all_forks = find_forks(all_rates)
      make_bets(all_forks)
    end

    private

    def get_rates
      rates = RatesGetter.new.rates_hash

      rates.tap { ActionCable.server.broadcast('rates_channel', rates_hash: rates) }
    end

    def find_forks(rates)

    end

    def make_bets(forks)

    end
  end
end