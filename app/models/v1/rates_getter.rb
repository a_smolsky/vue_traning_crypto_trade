# frozen_string_literal: true

require_relative 'rates/binance_rates_getter'
require_relative 'rates/poloniex_rates_getter'
require_relative 'rates/yo_bit_rates_getter'
require_relative 'rates/live_coin_rates_getter'
require_relative 'rates/exmo_rates_getter'
require_relative 'rates/rates_base'
require_relative 'rates/rates_object'

module V1
  class RatesGetter
    RATE_GETTERS = {
      binance: Rates::BinanceRatesGetter,
      poloniex: Rates::PoloniexRatesGetter,
      yo_bit: Rates::YoBitRatesGetter,
      live_coin: Rates::LiveCoinRatesGetter,
      exmo: Rates::ExmoRatesGetter
    }.freeze

    def rates_hash
      RATE_GETTERS
        .keys
        .each_with_object({}) { |key, data| data[key] = RATE_GETTERS[key].new.rates_get }
        .to_json
    end

    private

    attr_reader :creator_class
  end
end