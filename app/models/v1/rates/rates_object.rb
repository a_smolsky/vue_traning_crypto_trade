module V1
  module Rates
    class RatesObject
      ALLOWED_DEPTH = 0.1.freeze
      SAFE_CO = 1.5.freeze

      def initialize(api_rates)
        @rates = api_rates
        @sell_average, @sell_out, @sell_depth = prices(:sell)
        @buy_average, @buy_out, @buy_depth = prices(:buy)
      end

      private

      attr_reader :rates, :sell_average, :sell_out, :buy_average, :buy_out, :sell_depth, :buy_depth

      def prices(type)
        limited_rates = actual_rates(rates[type])

        sell_out = rates[type].map(&:first).first.round(4)
        average_price = (limited_rates.map(&:first).sum / limited_rates.size).round(4)
        depth = limited_rates.map(&:last).sum.round(6)

        [average_price, sell_out, depth]
      end

      def actual_rates(data_array)
        rates_storage = []

        data_array.each do |rate|
          depth = rates_storage.map(&:last).sum
          break if depth >= ALLOWED_DEPTH

          rates_storage << rate
        end

        rates_storage
      end
    end
  end
end
