module V1
  module Rates
    class RatesBase
      def rates_get
        RatesObject.new(api_rates)
      end

      private

      def validate_response(response)
        raise(StandardError, 'Unexpected response error') unless response.code == 200
        raise(StandardError, 'Unexpected data error') unless response.parsed_response.present?
      end

      def validate_response_body(error_flag, parsed_response)
        raise(StandardError, 'Unexpected data format error') unless parsed_response.is_a?(Hash)
        raise(StandardError, parsed_response[error_flag]) if parsed_response[error_flag]
      end

      def validate_data_integrity(data, asks = 'asks', bids = 'bids')
        valid =  data.present?
        valid &&= data[asks].all? { |ask| ask.is_a?(Array) }
        valid &&= data[bids].all? { |bids| bids.is_a?(Array) }

        raise(StandardError, 'Data integrity error') unless valid
      end
    end
  end
end