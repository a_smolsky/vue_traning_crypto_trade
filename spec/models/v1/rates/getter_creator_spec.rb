require 'models/v1/rates/getter_creator'

describe V1::Rates::GetterCreator do
  let(:rate_sources_mock) do
    {
      binance: double(rates: :binance_rates),
      poloniex: double(rates: :poloniex_rates),
      yo_bit: double(rates: :yo_bit_rates),
      live_coin: double(rates: :live_coin_rates),
      exmo: double(rates: :exmo_rates),
    }
  end

  before(:each) { allow_any_instance_of(described_class).to receive(:rate_sources).and_return(rate_sources_mock) }

  it 'returns binance rates' do
    expect(described_class.new(:binance).rates_get).to eq(:binance_rates)
  end

  it 'returns poloniex rates' do
    expect(described_class.new(:poloniex).rates_get).to eq(:poloniex_rates)
  end

  it 'returns yo_bit rates' do
    expect(described_class.new(:yo_bit).rates_get).to eq(:yo_bit_rates)
  end

  it 'returns live_coin rates' do
    expect(described_class.new(:live_coin).rates_get).to eq(:live_coin_rates)
  end

  it 'returns exmo rates' do
    expect(described_class.new(:exmo).rates_get).to eq(:exmo_rates)
  end

  it 'rases unknown source error' do
    source_key = :unexpected_source_key
    expected_error = "Unknown source '#{source_key}' requested"

    expect { described_class.new(source_key).rates_get }.to raise_error(StandardError, expected_error)
  end
end