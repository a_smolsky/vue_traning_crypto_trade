# frozen_string_literal: true

module V1
  module Rates
    class BinanceRatesGetter < RatesBase
      include HTTParty
      base_uri 'https://api.binance.com'
      CURRENCY_PAIR = 'BTCUSDT'.freeze

      private

      def api_rates
        api_data = call_rates

        {
          sell: api_data['asks'].map { |ask| ask.map(&:to_f) },
          buy: api_data['bids'].map { |bid| bid.map(&:to_f) }
        }
      end

      private

      def call_rates
        rates = self.class.get(rates_path, query: query)
        validate_response(rates)

        response_body = rates.parsed_response
        validate_response_body('msg', response_body)
        validate_data_integrity(response_body)

        response_body
      end

      def rates_path
        '/api/v3/depth'
      end

      def query
        {
          limit: 20,
          symbol: CURRENCY_PAIR
        }
      end
    end
  end
end
