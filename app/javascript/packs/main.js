import Vue from 'vue/dist/vue.js';
import BootstrapVue from 'bootstrap-vue';
import App from '../views/app.vue';
import axios from 'axios';

Vue.use(BootstrapVue);

new Vue({
    el: '#app',
    components: {
        vue_app: App
    },
    methods: {
        getRates() {
            setInterval(function() {
                axios.get('http://localhost:3000/v1/rates/index')
            }, 2000)
        }
    },
    mounted() {
        this.getRates();
    }
});