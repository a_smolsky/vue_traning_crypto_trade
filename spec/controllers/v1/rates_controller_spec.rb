require 'controllers/v1/rates_controller'

describe V1::RatesController do
  context 'get rates' do
    let(:expected_response_status) { 200 }
    let(:expected_response_type) { Hash }
    let(:expected_rate_keys) { %w[binance poloniex yo_bit live_coin exmo] }

    it 'returns rates in expected format' do
      get :index

      response_data = JSON.parse(response.body)

      expect(response.status).to eq(expected_response_status)
      expect(response_data).to  be_kind_of(Hash)
      expect(response_data.keys).to  eq(expected_rate_keys)
    end
  end
end