require 'models/v1/rates_getter'

describe V1::RatesGetter do
  subject { described_class.new( rate_getter_creator_class: rates_getter_creator_mock).rates_hash }

  let(:rates_getter_creator_mock) { class_double 'RatesGetterCreator' }
  let(:binance_getter_key) { :binance }
  let(:poloniex_getter_key) { :poloniex }
  let(:yo_bit_getter_key) { :yo_bit }
  let(:live_coin_getter_key) { :live_coin }
  let(:exmo_getter_key) { :exmo }
  let(:binance_getter) { double(rates_get: :binance_rates) }
  let(:poloniex_getter) { double(rates_get: :poloniex_rates) }
  let(:yo_bit_getter) { double(rates_get: :yo_bit_rates) }
  let(:live_coin_getter) { double(rates_get: :live_coin_rates) }
  let(:exmo_getter) { double(rates_get: :exmo_rates) }

  it 'gets expected rates' do
    expect(rates_getter_creator_mock).to receive(:new).with(binance_getter_key).and_return(binance_getter)
    expect(rates_getter_creator_mock).to receive(:new).with(poloniex_getter_key).and_return(poloniex_getter)
    expect(rates_getter_creator_mock).to receive(:new).with(yo_bit_getter_key).and_return(yo_bit_getter)
    expect(rates_getter_creator_mock).to receive(:new).with(live_coin_getter_key).and_return(live_coin_getter)
    expect(rates_getter_creator_mock).to receive(:new).with(exmo_getter_key).and_return(exmo_getter)

    expect(subject[binance_getter_key]).to eq(:binance_rates)
    expect(subject[poloniex_getter_key]).to eq(:poloniex_rates)
    expect(subject[yo_bit_getter_key]).to eq(:yo_bit_rates)
    expect(subject[live_coin_getter_key]).to eq(:live_coin_rates)
    expect(subject[exmo_getter_key]).to eq(:exmo_rates)
  end
end