# frozen_string_literal: true

module V1
  module Rates
    class YoBitRatesGetter < RatesBase
      include HTTParty
      base_uri 'https://yobit.net/api/3'
      CURRENCY_PAIR = 'btc_usd'.freeze

      private

      def api_rates
        api_data = call_rates

        {
          sell: api_data['asks'].map { |ask| ask.map(&:to_f) },
          buy: api_data['bids'].map { |bid| bid.map(&:to_f) }
        }
      end

      private

      def call_rates
        rates = self.class.get(rates_path, query: query)
        validate_response(rates)

        response_body = JSON.parse(rates.parsed_response)[CURRENCY_PAIR]
        validate_response_body('error', response_body)
        validate_data_integrity(response_body)

        response_body
      end

      def rates_path
        '/depth/' + CURRENCY_PAIR
      end

      def query
        { limit: 15 }
      end
    end
  end
end
